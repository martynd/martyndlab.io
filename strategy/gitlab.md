GitLab Developer Relations & Community Strategy
===============================================

Version 0.1 - Martyn Davies (martynrdavies@gmail.com)

# Overview

Build awareness and adoption of GitLab through a strategy of 4Cs with the aim of applying this both internally and externally.

The 4Cs are:
- Community
- Content
- Code
- Collaboration

We'll seek to target the following breakdown of users:


- **10% Longtail Developer**:
These are the students, hobbyists, makers who are early on
and exploring different technologies. They are the easiest to reach out to and the longest
to paid conversion. They can become loyal advocates of the product to their peers.

- **20% Startup Developer**:
These are the first technical hires at startups. Targeting these
developers at Accelerators is a great way to get usage at companies which are likely to grow fast. They are more likely to take risks on new technology.

- **25% MidSize Company Developer (QA Manger/Test Managers/Dev Ops)**:
No longer startups and have grown to 350+ employees. Have a more recent technology stack and are facing growing pains.

- **45% Enterprise Developers**:
These are the technical employees at big firms. They can
range from:
    - Developers working on Java/.NET stacks
    - Technical/Solution architects who fit the pieces together
    - DevOps/QA/Test Managers


How we'll measure:
------------------

We need to follow certain costs for the advocacy program to be scalable and successful.


**Enterprise Developer Acquisition (important for Gitlab EE adoption)**:

DevAcqCost < %X(tolerance factor of what percentage goes behind developer relations) * #Sales * MarginPerUnit

**Adoption for Gitlab CE**:

DevAcqCost < (%X ) * CustomerLifetimeValue


Put simply, we can't spend more than we'll make. However there is some flexibility here if certain acquisitions are strategic and will as a result generate additional LTV elsewhere.

# 4C Breakdown

## Community

**1. Give talks at Conferences/Meetups on related topics/languages. Also get booth’s at the Enterprise conferences**
	1.1 Language Confs(pyCon, NodeConf, jsconf, railsconf, javaOne, droidcon - everything is our playground)
	1.2 Enterprise(eg. Openstack Summit, OSCON)
	1.3 Regional Confs(Signal, CascadiaJS, DevCon London,  ..etc)
	1.4 Local Meetups(Language/git specific in most/all major cities and beyond)
	1.5 Partner Events(Talks/Demos at other Enterprise events)
	1.6 Collabs with other non competitive Developer Advocacy teams and their companies on/at events to maximise audience reach and legitimacy.

***Goals:***
- Build thought leadership.
- Enterprise Advocacy requires building relationships within a company and conference attendance and speaking is one of the best methods.
- GetLeads and relationships.
- Test if promo codes work at these events (system for giving out codes that come with $ value behind them)
- Raise overall awareness of GitLab as a collaboration tool
- Help events achieve good CI practice for their own outputs such as conference web presences.

***Measurements:***
1. *Awareness:*
- Number of people attending
- NPS of talk

2. *Adoption:*
- Number of relationships built
- Number of EE Leads passed on to sales
- Overall signups
- First Try Non Active Users
- Promo Codes (if applicable/alternative) used
- Promo conversions to paying customers
- Failed promo conversions(used but not upgraded)
- $$$ spent

3. *T+$E+$Sponsorship for events*


2. **Startup Accelerator CTO talks:**
2.1 Working with accelerator programmes is a great way to get in front of developers working on ideas that are *meant* to scale. They are also very much in need of help and support around collaboration, openness, continuous integration strategy and overall good working practices. Ideal for GitLab.

2.2 Visit the programme and speak with each cohort as early as possible - either do a 1-many talk or ideally host 1-1 office hours with technical team members (but this should not exclude people as collaboration across teams is important).

2.3 We offer special promo codes with a $ value behind it for use with GitLab EE, for startups in top tier Accelerator programs (fastest to raise+scale into a point of needing EE).

***Goals:***
- Get scaling early stage companies using GitLab as the core to their collaboration suite and CI. Prove that the suite is useful to more than just developers in a small but fast moving team.

***Measurements:***
Awareness:
- Number of startups spoken to.
- NPS of talk/session given
- Adoption

3. **Hackathons/Code Heavy Events** 

- Startup/Midsize (TechCrunch Disrupt/Signal/PHP London)
- Student (where not already supported by GitHub exclusively)
- Discovery (new technology, early stage, newbies)
- Enterprise Internal Hackathons (i.e: NBC Internal Hackathon, KPMG Internal Hackathon, Geovation Internal Hackathon)

*Internally at GitLab*
Company Hackathon during an offsite once a year. Helps with community and product knowledge as well as helping team members understand some of what developer advocacy does.

Guests from other companies can also join, and then may do the same with us at their own internal hackathons.

**Goals:**
- Build awareness of GitLab longtail and Startup+Midsize
developers.
- Build relationships with other sponsors and attending companies in a non competitive manner (swap tips).
- Internal hackathon: Builds community, product awareness and team output awareness within company and we also invite 3rd party companies to give demos or materials for use within GitLab.

**Measurements:**
*Awareness:*
- Number of people who watched a GitLab demo
- Number of people who attended any workshops given
- Adoption:
— Signups
— First Try Non Active Users (track WHAT they use it for)
— Retention post event (% loss/gain)
— Number of promo codes given (event dependent)
— Number of promo codes converted to paying customers
- Cost:
— Dollarz $$$

4. **Education Events/Coding Schools**

Host workshops teaching technologies in tech hubs, or coding schools. These can be GitLab specific but can be tailored to suit the given venue/audience. They should always be delivered from a place of ‘best practices’ and move towards the goal of GitLab being the *only* tool a developer would need for an end to end coding, testing, CI and server solution.

***Goals:***
Teach developers new technologies and practices

***Measurements:***
Awareness:
- Number of developers spoken to
- NPS of workshop

Adoption:
- Traffic to site
- Signups
- First Try Non Active Users (and again, tracking what they’re using it for)


5. **Monitor Online Social Media and answer questions/give guidance**:
- Hacker News
- Stack Overflow
- Twitter
- Facebook
- Set Google alerts

***Goals***:
- Give guidance to developers asking questions on these channels.
- Point back to docs or blog posts.
- Look out for heroes who are going above and beyond in the
community. Reward them with Swag and thank them (then maybe hire them).

***Measurements:***
Awareness:
- Number of support tickets fielded
- Number of community members rewarded

Adoption:
- Traffic to site referred to from these posts
- Number of subsequent signups
- Number of First Try Non Active Users (yes, and what they used it for.)

6. **Ambassador Program:**
Outside of an overall increase in staffing on the Developer Relations & Community Team, there’s an additional option that gives us the potential for even greater scale.

At SendGrid our ‘Ambassadors’ were typically students who love the product and were passionate about community and helping others, so we would effectively ‘sponsor’ their presence at events - they’ll wear special branded tees and help to create content, videos, and support other developers.

They act as mini advocates who we work work closely with, and not only mentor but to help progress as Dev Advocates.

This programme also acts as a great hiring pipeline.

***Goals:***
- Expand what we do to beyond our initial reach with current/projected staffing
- Recognize community members who go above and beyond with the ability to become Ambassadors.
- Best success with college/uni students active in the Hackathon
community
- We reward them with experiences and traveling to new cities.
- Gifts for every event that they help out with. We can also pay the more senior ambassadors who are experienced enough to handle events on their own (if we decide to).

***Measurements:***
- Traffic to blog and docs from cities and dates from where these Ambassadors attend meetups or events
- Number Signups
- Number of additional events we can to attend due to this programme


## Code
- Tools that make developers lives easier and reduces TTFHW (Time to first hello world)
- Full fledged sample apps/mashups that developers can copy and paste into their own code
- Tools that encourage open source contribution and engage our users:
— for example, let’s automate swag being send after every pull request gets merged

***Goals:***
- The projects that we create become the material for our content and talks.
- We build tools and projects at the intersection of GitLab products, our passion and what makes developer’s lives easier and team collaboration easier.
- Make projects interesting and creative so as to create the highest chance of virality
- Dogwood - use the GitLab suite as much as possible, especially in video form.

***Measurements:***
- Traffic our projects receive
- Pieces of content created
- Signups
- Wow moments
- Number of external references

## Content
We create content that inspires developers to build with GitLab at the core of their stack and gives them the tools and knowledge they need to make their lives easier.

We should ALWAYS try to use projects on GitLab as the core for our examples. Highlighting users will help drive further community adoption.

Also educates on open standards, open source, cross team collaboration and transparency.

Work with other third parties to showcase the commonly used problems that GitLab and a 3rd party solve - it’s just DOUBLE the reasons to talk about us.

Create as much content as we can through doing the day to day - DOCUMENT EVERYTHING.

- Webinars
- Screencasts
- Blog Posts

We should also experiment with other channels such as doing live shows on Twitch/YouTube Live. Hosting Google Hangouts people can join for Q&A sessions.

We should create *the* leading podcast on technology collaboration.



***Goals:***
- Drive Signups and upgrades. All content should also lead to docs due to it having the highest source of conversions.

***Measurements:***
*Awareness:*
- Traffic hitting content
- Technology Stack of Content

*Adoption:*
- Number of Signups
- Number of First Try Non Active Users

## Collaboration

The faster way to scale this overall effort and the awareness of GitLab is to partner with other technology companies doing interesting things (Auth0, SendGrid, KeenIO, Nexmo, Twilio, Clarifai etc…)

Working together to form events and content gives us instant access to new audiences we might not get otherwise, and an additional nod of legitimacy through association.

We can also speak to ~twice the audience without doubling the cost. Shared effort all the way.
